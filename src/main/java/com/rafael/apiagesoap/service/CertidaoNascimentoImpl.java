package com.rafael.apiagesoap.service;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;
import static java.util.Calendar.MONTH;
import static java.util.Calendar.YEAR;
import static java.util.Calendar.DAY_OF_MONTH;
import static java.util.Calendar.DAY_OF_WEEK;
import static java.util.Calendar.getInstance;

import jakarta.jws.WebService;

@WebService(endpointInterface = "com.rafael.apiagesoap.service.CertidaoNascimento")
public class CertidaoNascimentoImpl implements CertidaoNascimento {

	@Override
	public int calcularIdade(String idade) {
		SimpleDateFormat sdf = new SimpleDateFormat("dd/MM/yyyy");
		int idadeCalculada = 0;
		try {
			Calendar dataNascimento = getInstance();
			Date idadeDate = sdf.parse(idade);
			dataNascimento.setTime(idadeDate);

			Calendar hoje = getInstance();
			idadeCalculada = hoje.get(YEAR) - dataNascimento.get(YEAR);

			idadeCalculada = isMesDeAniversario(idadeCalculada, dataNascimento, hoje);
		} catch (ParseException e) {
			e.printStackTrace();
		}
		return idadeCalculada;
	}

	private int isMesDeAniversario(int idadeCalculada, Calendar dataNascimento, Calendar hoje) {
		if (hoje.get(MONTH) < dataNascimento.get(MONTH)) {
			idadeCalculada--;
		} else {
			if (hoje.get(Calendar.MONTH) == dataNascimento.get(MONTH)
					&& hoje.get(DAY_OF_MONTH) < dataNascimento.get(DAY_OF_MONTH)) {
				idadeCalculada--;
			}
		}
		return idadeCalculada;
	}

	@Override
	public String diaSemanaNascimento(String idade) {
		String dias[] = { "Domingo", "Segunda", "Terça", "Quarta", "Quinta", "Sexta", "Sábado" };
		int dia = 0;
		SimpleDateFormat sdf = new SimpleDateFormat("dd/MM/yyyy");
		try {
			Calendar dataNascimento = getInstance();
			Date idadeDate = sdf.parse(idade);
			dataNascimento.setTime(idadeDate);
			dia = dataNascimento.get(DAY_OF_WEEK);
		} catch (ParseException e) {
			e.printStackTrace();
		}
		return dias[dia - 1];
	}

}
