package com.rafael.apiagesoap;

import com.rafael.apiagesoap.service.CertidaoNascimento;
import com.rafael.apiagesoap.service.CertidaoNascimentoImpl;

import static jakarta.xml.ws.Endpoint.publish;

public class ApiAgeSoap {

	public static void main(String[] args) {
		
		String path = "http://localhost:8081/";
		CertidaoNascimento certidaoNascimento = new CertidaoNascimentoImpl();
	    publish(path + "certidaonascimento", certidaoNascimento);
	    
	    System.out.println("Serviço publicado com sucesso");
		
	}

	
}
